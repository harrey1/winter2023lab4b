import java.util.Scanner;
public class VirtualPetApp {
	public static void main (String[] args){
		Lion[] pride = new Lion[1];
		
		Scanner reader = new Scanner(System.in);
		
		for (int i = 0; i < pride.length; i++){
			
			
			System.out.println("Enter the origin country of your lion: ");
			String country = reader.nextLine();
			
			System.out.println("Enter the max speed (in Km/h) of your lion: ");
			int maxSpeed = Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter the mass (in Kg) of your lion: ");
			int mass = Integer.parseInt(reader.nextLine());
			
			pride[i] = new Lion(country, maxSpeed, mass);
		}
		//prints the animal's data using getters
		System.out.println("Here is your last lion's data: ");
		System.out.println("Country: " + pride[0].getOriginCountry());
		System.out.println(pride[0].getMaxSpeed() + " Km/h");
		System.out.println(pride[0].getMass() + " Kg");
		
		System.out.println("Pick another mass");
		pride[0].setMass(Integer.parseInt(reader.nextLine()));
		
		//prints the animal's data after modifying one of the data using setter
		System.out.println("Here is your last lion's data: ");
		System.out.println("Country: " + pride[0].getOriginCountry());
		System.out.println(pride[0].getMaxSpeed() + " Km/h");
		System.out.println(pride[0].getMass() + " Kg");
		
	}
}