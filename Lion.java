public class Lion{
	private String originCountry;
	private int maxSpeed;
	private int mass;
	
	//constructor
	public Lion(String originCountry, int maxSpeed, int mass){
		this.originCountry = originCountry;
		this.maxSpeed = maxSpeed;
		this.mass = mass;
	}
	
	public void setMass(int mass){
		this.mass = mass;
	}
	//getter methods
	public String getOriginCountry(){
		return this.originCountry;
	}
	
	public int getMaxSpeed(){
		return this.maxSpeed;
	}
	
	public int getMass(){
		return this.mass;
	}
	
	public void loudRoar(){
		System.out.println("ROAAARRRRRRRR");
	}
	
	public void goHunt(){
		if(this.maxSpeed < 40 && this.mass > 220){
			System.out.println("This lion seems to not be able to go hunt :(");
		}
		else
			System.out.println("This lion will go hunt and bring food to his pride :)");
	}
}